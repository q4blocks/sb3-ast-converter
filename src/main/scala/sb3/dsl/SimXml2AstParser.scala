package sb3.dsl
import scala.xml._
import scala.xml.Utility.trim
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import ast.{ List => ASTList, _ }
import sb3.data.ProcDeclMeta
import sb3.data.ParamDeclMeta
import sb3.data.ProcAccMeta
import sb3.data.Default

/**
 * simple parser to construct AST dsl in xml format to
 */
class SimXml2AstParser {
  def parse(xmlStr: String): Any = {
    val xmlNode: Node = scala.xml.XML.loadString(xmlStr)
    parse(xmlNode)
  }

  def parse(xml: Node): Any = {
    trim(xml) match {
      case <num>{ value }</num> => new NumLiteral(value.toString())
      case <str>{ value }</str> => new StrLiteral(value.toString())
      case <attr>{ id }</attr> => new AttrAccess(id.toString())
      case exprNode @ <expr>{ _* }</expr> => parseExpr(exprNode)
      case stmtNode @ <stmt>{ _* }</stmt> => parseStmt(stmtNode)
      case callNode @ <call>{ _* }</call> => parseCall(callNode)
      case assignNode @ <assign>{ _* }</assign> => parseAssignment(assignNode)
      case incrementNode @ <increment>{ _* }</increment> => parseIncrement(incrementNode)
      case stopNode @ <stop/> => parseStopStmt(stopNode)
      case ifelseNode @ <ifelse>{ _* }</ifelse> => parseIfElseStmt(ifelseNode)
      case loopNode @ <loop>{ _* }</loop> => parseLoopStmt(loopNode)
      case scriptNode @ <script>{ _* }</script> => parseScript(scriptNode)
      case procDeclNode @ <procedure>{ _* }</procedure> => parseProcedure(procDeclNode)
      case spriteNode @ <sprite>{ _* }</sprite> => parseSprite(spriteNode)
      case broadcastNode @ <broadcast>{ _* }</broadcast> => parseBroadcast(broadcastNode)
      case receiveNode @ <receive/> => parseReceive(receiveNode)
      case programNode @ <program>{ _* }</program> => parseProgram(programNode)
      case _ => throw new Exception("Unknown xml node: " + xml)
    }
  }

  def parseExpr(xml: Node): Expr = xml match {
    case <num>{ value }</num> => new NumLiteral(value.toString())
    case <str>{ value }</str> => new StrLiteral(value.toString())
    case <attr>{ id }</attr> => new AttrAccess(id.toString())
    case xmlNode @ <varacc/> => parseVarAcc(xmlNode)
    case xmlNode @ <expr>{ children @ _* }</expr> => {
      val opExpr = new OperatorExpr()
      val opcode = xmlNode.attribute("type").map(_.toString)

      opExpr.setOpcode(opcode.getOrElse("expr"));
      val blockId = xmlNode.attribute("id").map(_.toString)
      if (blockId.isDefined) opExpr.setBlockId(blockId.get);
      var args: List[Expr] = children.map(child => parseExpr(child)).toList.filter(Option(_).isDefined)
      opExpr.initDefault(args.size)
      args.foreach(opExpr.addArg)
      for (i <- 0 until args.length) {
        val defaultVal: Default = new Default
        args.get(i) match {
          case lit: Literal => defaultVal.setFieldValue(lit.getValue)
          case nonlitExpr: Expr => defaultVal.setFieldValue("0")
          case _ => null
        }
        defaultVal.setShadowAttr(Map("type" -> "text"));
        defaultVal.setFieldAttr(Map("name" -> "TEXT"));
        opExpr.setDefault(defaultVal, i)
      }

      opExpr
    }
    case _ => throw new Exception("Unknown child elements in block ")
  }

  def parseStmt(xml: Node): Stmt = xml match {
    case xmlNode @ <stmt>{ children @ _* }</stmt> => {
      val stmt = new ExprStmt()
      val opcode = xmlNode.attribute("type").map(_.toString)
      stmt.setOpcode(opcode.getOrElse("stmt"));
      val blockId = xmlNode.attribute("id").map(_.toString)
      if (blockId.isDefined) stmt.setBlockId(blockId.get);
      var args: List[Expr] = children.map(child => parseExpr(child)).toList.filter(Option(_).isDefined)
      args.foreach(stmt.addArg)
      stmt.initDefault(args.size)
      for (ai <- 0 until args.size) {
        stmt.setDefault(new Default("0"), ai)
      }
      stmt
    }
    case xmlNode @ <ifelse>{ children @ _* }</ifelse> => parseIfElseStmt(xmlNode)
    case _ => throw new Exception("Unknown child elements in block ")
  }

  def parseAssignment(xml: Node): AssignStmt = xml match {
    case xmlNode @ <assign>{ children @ _* }</assign> => {

      val assignment = new AssignStmt()
      assignment.setBlockId(xmlNode.attribute("id").map(_.toString).getOrElse(""))
      assignment.setOpcode("data_setvariableto")
      children.map(child => child match {
        case <dest>{ varAcc }</dest> => assignment.setDest(parseVarAcc(varAcc))
        case <src>{ srcExpr }</src> => assignment.setSource(parseExpr(srcExpr))
        case _ => new Exception("assignment: invalid child elements")
      })
      // TODO: default value for source
      assignment.initDefault(2);
      assignment.setDefault(new Default("0"), 1)
      assignment
    }
    case _ => throw new Exception("Invalid assignment statement syntax")
  }

  def parseIncrement(xml: Node): AssignStmt = xml match {
    case xmlNode @ <increment>{ children @ _* }</increment> => {

      val increment = new IncrementStmt()
      increment.setBlockId(xmlNode.attribute("id").map(_.toString).getOrElse(""))
      increment.setOpcode("data_changevariableby")
      children.map(child => child match {
        case <dest>{ varAcc }</dest> => increment.setDest(parseVarAcc(varAcc))
        case <src>{ srcExpr }</src> => increment.setSource(parseExpr(srcExpr))
        case _ => new Exception("IncrementStmt: invalid child elements")
      })
      increment.initDefault(2);
      increment.setDefault(new Default("0"), 1)
      increment
    }
    case _ => throw new Exception("Invalid increment statement syntax")
  }

  def parseStmtSeq(stmts: NodeSeq): BlockSeq = {
    val blockSeq = new BlockSeq
    stmts.foreach(stmt => blockSeq.addStmt(parse(stmt).asInstanceOf[Stmt]))
    blockSeq
  }

  def parseIfElseStmt(xml: Node): Stmt = xml match {
    case xmlNode @ <ifelse>{ children @ _* }</ifelse> => {
      val stmt = new IfElseStmt()
      stmt.setBlockId(xmlNode.attribute("id").map(_.toString).getOrElse(""))
      val opcode = xmlNode.attribute("type").map(_.toString)
      stmt.setOpcode(opcode.getOrElse("control_if_else"));
      var args = children.map(child => child match {
        case <cond>{ expr }</cond> => stmt.setCond(parseExpr(expr))
        case <then>{ stmts @ _* }</then> => stmt.setThen(parseStmtSeq(stmts))
        case <else>{ stmts @ _* }</else> => stmt.setElse(parseStmtSeq(stmts))
        case _ => throw new Exception("Unexpected AST element: " + child)
      })
      stmt
    }
    case _ => throw new Exception("Unknown child elements in block ")
  }

  def parseLoopStmt(xml: Node): Stmt = xml match {
    case xmlNode @ <loop>{ children @ _* }</loop> => {
      val stmt = new LoopStmt()
      stmt.setBlockId(xmlNode.attribute("id").map(_.toString).getOrElse(""))
      val opcode = xmlNode.attribute("type").map(_.toString)
      stmt.setOpcode(opcode.getOrElse("control_repeat")); //default is repeat
      var args = children.map(child => child match {
        case <cond>{ expr }</cond> => stmt.setCond(parseExpr(expr))
        case <body>{ stmts @ _* }</body> => stmt.setBody(parseStmtSeq(stmts))
        case _ => throw new Exception("Unexpected AST element: " + child)
      })
      stmt.initDefault(2)
      stmt.setDefault(new Default("0"), 0)
      stmt
    }
    case _ => throw new Exception("Unknown child elements in block ")
  }

  def parseStopStmt(xml: Node): Stmt = {
    val stopStmt = new StopStmt
    stopStmt.setOpcode("control_stop")
    stopStmt.setBlockId(xml.attribute("id").map(_.toString).get)
    stopStmt.setArg(new AttrAccess("this script"), 0)
    stopStmt
  }

  def parseScript(xml: Node): Script = xml match {
    case xmlNode @ <script>{ stmts @ _* }</script> => {
      val script = new Script
      val id = xmlNode.attributes.get("id").map(_.toString).getOrElse(null)
      script.setId(id)
      script.setBody(parseStmtSeq(stmts))
      script
    }
    case _ => throw new Exception("Unknown child elements in script")
  }

  def parseVarDecl(xml: Node): VarDecl = xml match {
    case node @ <vardecl/> => {
      val varDecl = new VarDecl(node.attributes.get("id").map(_.toString).get)
      varDecl.setName(varDecl.getID)
      varDecl
    }
    case _ => throw new Exception("Unexpected VarDecl element: " + xml)
  }

  def parseVars(xml: Node): List[VarDecl] = xml match {
    case <vars>{ vardecls @ _* }</vars> => vardecls.map(parseVarDecl(_)).toList
  }

  def parseVarAcc(xml: Node): VarAccess = xml match {
    case node @ <varacc/> => {
      val varAcc = new VarAccess(node.attributes.get("vid").map(_.toString).get)
      varAcc.setBlockId(node.attributes.get("id").orElse(Option("")).map(_.toString).get)
      varAcc.setName(varAcc.getID)
      varAcc
    }
  }

  def parseScriptableHelper(scriptable: Scriptable, children: scala.xml.NodeSeq) = {
    children.filter(child => child.label == "vars")
      .collectFirst { case vars => parseVars(vars).foreach(scriptable.addVarDecl(_)) }
    children.filter(child => child.label == "script")
      .foreach(script => scriptable.addScript(parseScript(script)))
    children.filter(child => child.label == "procedure")
      .foreach(proc => scriptable.addProcDecl(parseProcedure(proc)))
  }

  def parseSprite(xml: Node): Sprite = xml match {
    case xmlNode @ <sprite>{ children @ _* }</sprite> => {
      val sprite = new Sprite()
      val name = xmlNode.attribute("name").map(_.toString).getOrElse("Anonymous")
      sprite.setName(name)
      parseScriptableHelper(sprite, children)
      sprite
    }
  }

  def parseStage(xml: Node): Stage = xml match {
    case xmlNode @ <stage>{ children @ _* }</stage> => {
      val stage = new Stage()
      parseScriptableHelper(stage, children)
      stage
    }
  }

  def parseProgram(xml: Node): Program = xml match {
    case xmlNode @ <program>{ children @ _* }</program> => {
      val program = new Program()
      children.filter(child => child.label == "stage")
        .collectFirst { case stage => program.setStage(parseStage(stage)) }
      if (program.getStage == null) program.setStage(new Stage())
      children.filter(child => child.label == "vars")
        .collectFirst { case vars => parseVars(vars).foreach(program.getStage.addVarDecl(_)) }
      children.filter(child => child.label == "script")
        .foreach(script => program.getStage.addScript(parseScript(script)))
      children.filter(child => child.label == "sprite")
        .foreach(sprite => program.getStage.addSprite(parseSprite(sprite)))
      program
    }
  }

  def parseParams(xml: Node): List[ParamDecl] = xml match {
    case <params>{ paramdecls @ _* }</params> => paramdecls.map(parseParamDecl(_)).toList
  }

  def parseParamDecl(xml: Node): ParamDecl = xml match {
    case node @ <param/> => {
      val paramDecl = new ParamDecl(node.attributes.get("id").map(_.toString).get)
      paramDecl.setName(paramDecl.getID)
      paramDecl.setMeta(new ParamDeclMeta.Builder(paramDecl.getName)
        .build())
      paramDecl
    }
    case _ => throw new Exception("Unexpected ParamDecl element: " + xml)
  }

  def parseProcedure(xml: Node): ProcDecl = xml match {
    case xml @ <procedure>{ children @ _* }</procedure> => {
      val procDecl = new ProcDecl

      children.filter(child => child.label == "params")
        .collectFirst { case params => parseParams(params).foreach(procDecl.addParamDecl(_)) }

      procDecl.setMeta(new ProcDeclMeta.Builder()
        .procCode(xml.attribute("proccode").map(_.toString).getOrElse("Anonymous"))
        .build())

      children.filter(child => child.label == "body")
        .collectFirst { case <body>{ stmts @ _* }</body> => procDecl.setBody(parseStmtSeq(stmts)) }

      return procDecl
    }
  }

  def parseCall(xml: Node): ProcAccess = xml match {
    case xmlNode @ <call>{ children @ _* }</call> => {
      val procAcc = new ProcAccess()
      procAcc.setMeta(new ProcAccMeta())
      val blockId = xmlNode.attributes.get("id").map(_.toString).getOrElse(null)
      val proccode = xmlNode.attributes.get("proccode").map(_.toString).getOrElse(null)
      procAcc.setBlockId(blockId)
      procAcc.getMeta.setProccode(proccode)
      children.filter(child => child.label == "args")
        .collectFirst {
          case args => parseCallArgs(args).foreach {
            case (id: String, expr: Expr) => {
              procAcc.getMeta.getArgumentIds.add(id)
              procAcc.addArg(expr)
            }
          }
        }
      procAcc
    }
  }

  def parseCallArgs(xml: Node): List[(String, Expr)] = xml match {
    case <args>{ args @ _* }</args> => args.map(parseCallArg(_)).toList
  }

  def parseCallArg(xml: Node): (String, Expr) = xml match {
    case node @ <arg>{ exprNode }</arg> => {
      val expr = parseExpr(exprNode)
      //what to do with param id :
      val argId = node.attributes.get("id").map(_.toString).getOrElse(null)
      (argId, expr)
    }
    case _ => throw new Exception("Unexpected ParamDecl element: " + xml)
  }

  def parseBroadcast(xml: Node): BroadcastStmt = xml match {
    case xmlNode @ <broadcast>{ exprNode }</broadcast> => {
      val broadcast = new BroadcastStmt()
      val wait = xmlNode.attribute("wait").map(_.toString).getOrElse("false")
      val msg = parseExpr(exprNode)
      val id = xmlNode.attributes.get("id").map(_.toString).getOrElse(null)
      broadcast.setBlockId(id)
      broadcast.setOpcode(if (wait == "false") "event_broadcast" else "event_broadcastandwait")
      broadcast.setMsg(parseExpr(exprNode))
      broadcast
    }
  }

  def parseReceive(xml: Node): ReceiveStmt = xml match {
    case xmlNode @ <receive/> => {
      val receive = new ReceiveStmt()
      val msg = xmlNode.attribute("msg").map(_.toString).getOrElse(throw new Exception("no msg attribute in receive stmt"))
      val id = xmlNode.attributes.get("id").map(_.toString).getOrElse(null)
      receive.setBlockId(id)
      receive.setOpcode("event_whenbroadcastreceived")
      val msgVar = new VarAccess(msg)
      msgVar.setName(msg)
      msgVar.setType("")
      receive.setMsg(msgVar)
      receive
    }
  }
}

object SimXml2AstParser extends App {

}