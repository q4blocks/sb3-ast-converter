package sb3.parser
import ast.{ List => ASTList, _ }
import sb3.data.{ Default }
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import scala.xml.Text
import sb3.parser.Block2Ast._ //import so implicit conversion works
import sb3.data.ProcDeclMeta
import sb3.data.ParamDeclMeta
import sb3.data.ProcAccMeta
import play.api.libs.json._
import scala.xml.Utility.trim
/*
 * functionalities to convert from block elements to ASTNode and back to Xml
 */

trait Elm2Xml {
  def toXml(): scala.xml.Node
}

//trait Elm2Ast[T <: ASTNode[_]] {
trait Elm2Ast[T <: Any] {
  def toAst(): T
}

trait SomeAst[Any] {
  def someAst(): Any
}

//trait SomeAst

abstract class Arg extends Elm2Xml {
  val attr: Map[String, String]
}

case class ProgramEl(stage: StageEl) extends Elm2Xml with Elm2Ast[Program] {
  override def toXml() = {
    <program>{ stage.toXml }{
      stage.sprites.map(sprite => {
        <sprite name={ sprite.name }>{ sprite.toXml }</sprite>
      })
    }</program>
  }
  override def toAst() = {
    val program = new Program()
    program.setStage(stage.toAst())
    program
  }
}
case class StageEl(costumes: List[CostumeEl] = List.empty[CostumeEl], vars: List[VariableEl], scripts: List[ScriptEl], procDefs: List[ProcDefEl] = List.empty[ProcDefEl], sprites: List[SpriteEl]) extends Elm2Xml with Elm2Ast[Stage] {
  override def toXml() = <stage><xml><variables>{ vars.map(v => v.toXml) }</variables>{ procDefs.map(procDef => procDef.toXml) }{ scripts.map(script => script.toXml) }</xml></stage>
  override def toAst() = {
    val stage = new Stage()
    costumes.foreach(c => stage.addCostume(c.toAst))
    vars.foreach(v => stage.addVarDecl(v.toAst))
    procDefs.foreach(pd => stage.addProcDecl(pd.toProcDecl()))
    scripts.foreach(scriptEl => stage.addScript(scriptEl.toAst))
    sprites.foreach(spriteEl => stage.addSprite(spriteEl.toAst()))
    stage
  }
}

case class CostumeEl(name: String) extends Elm2Xml with Elm2Ast[Costume] {
  override def toXml() = <costume name={ name }/>
  override def toAst() = new Costume(name)
}

case class VariableEl(varType: String = "", id: String, name: String) extends Elm2Xml with Elm2Ast[VarDecl] {
  override def toXml() = <variable type={ varType } id={ id }>{ name }</variable>
  override def toAst() = {
    val varDecl = new VarDecl(id)
    varDecl.setName(name)
    varDecl.setVarType(varType)
    varDecl
  }
}

case class SpriteEl(name: String, attr: Map[String, String]= Map(), costumes: List[CostumeEl] = List.empty[CostumeEl], vars: List[VariableEl] = List.empty[VariableEl], procDefs: List[ProcDefEl] = List.empty[ProcDefEl], scripts: List[ScriptEl]) extends Elm2Xml with Elm2Ast[Sprite] {
  def toSprite() = new Sprite()
  override def toXml() = {
    <xml><variables>{ vars.map(v => v.toXml) }</variables>{ procDefs.map(procDef => procDef.toXml) }{ scripts.map(script => script.toXml) }</xml>
  }
  override def toAst() = {
    val sprite = new Sprite()
    sprite.setName(name)
    sprite.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava)
    costumes.foreach(c => sprite.addCostume(c.toAst))
    vars.foreach(v => sprite.addVarDecl(v.toAst))
    procDefs.foreach(pd => sprite.addProcDecl(pd.toProcDecl()))
    scripts.foreach(scriptEl => sprite.addScript(scriptEl.toAst))
    sprite
  }
}

case class ProcDefEl(attr: Map[String, String], signature: Value, seq: Option[BlockSeqEl]) extends Elm2Xml with Elm2Ast[ProcDecl] {

  def toProcDecl() = {
    val procDecl = new ProcDecl()
    procDecl.setOpcode(attr.get("type").get)
    procDecl.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava)
    val sigValue = signature

    val shadowEl = sigValue.shadow.get
    //combine procdef's shadow info with mutation info
    shadowEl.args.filter(arg => arg.isInstanceOf[Mutation]).collectFirst {
      case mut: Mutation => procDecl.setMeta(new ProcDeclMeta.Builder()
        .shadowId(shadowEl.attr.get("id").getOrElse(getRandomId()))
        .procCode(mut.attr.get("proccode").getOrElse(throw new Exception("no proccode ")))
        .argIds(ParsingUtil.extractListOfString(mut.attr.get("argumentids").getOrElse("[]")))
        .argNames(ParsingUtil.extractListOfString(mut.attr.get("argumentnames").getOrElse("[]")))
        .argDefaults(ParsingUtil.extractObjList(mut.attr.get("argumentdefaults").getOrElse("[]")))
        .build())
    }

    sigValue.shadow.get.args.filter(arg => arg.isInstanceOf[Value])
      .collect {
        case value: Value => {
          val shadowEl = value.shadow.get
          val meta = new ParamDeclMeta.Builder(value.attr.get("name").get)
            .shadowId(shadowEl.attr.get("id").get)
            .shadowType(shadowEl.attr.get("type").get)
            .fieldName(shadowEl.args.flatMap { case f: Field => f.attr.get("name") }.collectFirst { case value => value }.get)
            .fieldValue(shadowEl.args.map { case f: Field => f.value }.collectFirst { case value => value }.get)
            .build()
          val paramDecl = new ParamDecl()
          paramDecl.setMeta(meta)
          procDecl.addParamDecl(paramDecl)
        }
      }
    // when value elements are not provided
    if (procDecl.getMeta.getArgIds.size() > sigValue.shadow.get.args.filter(arg => arg.isInstanceOf[Value]).size) {
      for (i <- 0 until procDecl.getMeta.getArgIds.size()) {
        val paramDecl = new ParamDecl()
        val meta = new ParamDeclMeta.Builder(procDecl.getMeta.getArgNames.get(i))
          .fieldName("VALUE")
          .fieldValue(procDecl.getMeta.getArgIds.get(i)).build()
        paramDecl.setMeta(meta)
        paramDecl.setID(meta.getFieldValue)
        paramDecl.setName(meta.getName)

        procDecl.addParamDecl(paramDecl)
      }
    }

    procDecl.setBody(if (seq.isDefined) seq.get.toAst() else new BlockSeq)

    procDecl
  }
  override def toAst() = toProcDecl()
  override def toXml() = trim(BlockEl(attr, List(Some(signature)), if (seq.isDefined) Some(seq.get.topBlock) else None).toBlockSeqXml())
}

case class ScriptEl(seq: BlockSeqEl) extends Elm2Xml with Elm2Ast[Script] {
  def toScript() = new Script(seq.toAst())
  override def toXml() = seq.toXml()
  override def toAst() = toScript()
}

case class BlockSeqEl(topBlock: BlockEl) extends Elm2Xml with Elm2Ast[BlockSeq] {
  override def toAst() = topBlock.toBlockSeq()
  override def toXml() = topBlock.toBlockSeqXml()
}

case class Shadow(attr: Map[String, String] = Map(), args: List[Arg]) extends Elm2Xml {
  def toDefault(): Default = args(0) match {
    case field: Field => {
      val defaultVal = new Default()
      defaultVal.setShadowAttr(collection.mutable.Map(attr.toSeq: _*).asJava)
      defaultVal.setFieldAttr(collection.mutable.Map(field.attr.toSeq: _*).asJava)
      defaultVal.setFieldValue(field.value)

      defaultVal
    }
    case _ => throw new Exception("Expect Field as the first argument of Shadow but found: " + args(0))
  }

  override def toXml() =
    <shadow type={ optTxt(attr, "type") } id={ optTxt(attr, "id") }>{ { args.map(arg => arg.toXml) } }</shadow>

  def toAst(): Literal = {
    val res = new StrLiteral(args(0).asInstanceOf[Field].value)
    res.getAttrs.put("type", args(0).attr.get("type").getOrElse(null))
    res
  }

}
case class Value(attr: Map[String, String], shadow: Option[Shadow], block: Option[BlockEl] = None) extends Arg {
  def toAstArg(): (Option[Default], Option[Expr]) = this match {
    case Value(attr, Some(shadow), None) => (Some(shadow.toDefault()), Some(shadow.toAst()))
    case Value(attr, None, Some(block)) => (None, Some(block.toAst().asInstanceOf[OperatorExpr]))
    case Value(attr, Some(shadow), Some(block)) => (Some(shadow.toDefault()), Some(block.toAst().asInstanceOf[OperatorExpr]))
    case _ => throw new Exception("Malformed Value")
  }

  def toExprNodeDefaultPair(): (Expr, Default) = {
    val default = if (shadow.isDefined) shadow.get.toDefault() else null
    val expr = if (block.isDefined) block.get.toAst().asInstanceOf[Expr]
    else if (shadow.isDefined) default.toExpr();
    else null

    (expr, default)
  }

  override def toXml() = this match {
    case Value(attr, Some(shadow), None) => <value name={ optTxt(attr, "name") }>{ shadow.toXml }</value>
    case Value(attr, Some(shadow), Some(block)) => <value name={ optTxt(attr, "name") }>{ shadow.toXml }{ block.toXml }</value>
    case Value(attr, None, Some(block)) => <value name={ optTxt(attr, "name") }>{ block.toXml }</value>
    case _ => throw new Exception("Invalid AST state:" + this)
  }
}

case class Field(attr: Map[String, String] = Map(), value: String) extends Arg with Elm2Xml with Elm2Ast[Expr] {
  def toAst() = {
    if (attr.containsKey("variabletype")) {
      val varAcc = new VarAccess()
      varAcc.setType(attr.get("variabletype").get)
      varAcc.setName(value)
      varAcc.setID(attr.get("id").get)
      varAcc
    } else new AttrAccess(value) //block's top-level field is an attribute, if not top-level they are defaults/shadow

  }
  override def toXml() = {
    val varType = if (attr.getOrDefault("name", "").equals("VARIABLE")) reqTxt(attr, "variabletype", "") else optTxt(attr, "variabletype")
    <field name={ optTxt(attr, "name") } id={ optTxt(attr, "id") } variabletype={ varType }>{ value }</field>
  }
}

case class Mutation(attr: Map[String, String] = Map()) extends Arg with Elm2Xml {
  override def toXml() = {
    if (attr.containsKey("proccode")) {
      <mutation proccode={ optTxt(attr, "proccode") } argumentids={ attr.get("argumentids").get } argumentnames={ optTxt(attr, "argumentnames") } argumentdefaults={ optTxt(attr, "argumentdefaults") } warp={ optTxt(attr, "warp") }></mutation>
    } else if (attr.containsKey("hasnext")) {
      <mutation hasnext="false"></mutation>
    } else {
      throw new Exception("unknown mutation")
    }

  }
}

case class StatementEl(attr: Map[String, String], block: Option[BlockEl] = None) extends Arg with Elm2Xml with Elm2Ast[BlockSeq] {
  def toAst(): BlockSeq = if (block.isDefined) block.get.toBlockSeq() else new BlockSeq()
  override def toXml() = {
    if (block.isDefined) {
      <statement name={ optTxt(attr, "name") }>{ block.get.toBlockSeqXml() }</statement>
    } else {
      new scala.xml.Text("")
    }
  }
}

case class BlockEl(attr: Map[String, String], args: List[Option[Arg]], next: Option[BlockEl]) extends Elm2Xml with Elm2Ast[ScratchBlock] {
  val ExprBlock = """(expr.*|operator_.*|colour_.*||data_(?!variable|setvariableto|changevariableby|addtolist|deleteoflist|insertatlist|replaceitemoflist|showvariable|hidevariable|showlist|hidelist).*|motion_xposition|motion_yposition|motion_direction|looks_costumenumbername|looks_backdropnumbername|looks_size|sensing_(?!setdragmode|resettimer|askandwait).*|sound_volume|music_getTempo)""".r
  val LoopBlock = """(control_(?:repeat|forever|repeat_until))""".r
  val IfElseBlock = """(control_(?:if|if_else|)+)""".r
  val ControlStopBlock = """(control_stop)""".r
  val SimpleControlBlock = """(control_.*)""".r
  val StmtBlock = """(stmt.*|motion_.*|looks_.*|data_.*|sensing_.*|sound_.*|pen_.*|music_(?!getTempo).*|control_wait|control_wait_until)""".r
  val ProcCall = """(procedures_call)""".r
  val EventStartBlock = """(event_(?!broadcast|broadcastandwait|whenbroadcastreceived).*)""".r
  val ReceiverBlock = """(event_whenbroadcastreceived)""".r
  val BroadcastBlock = """(event_broadcast|event_broadcastandwait)""".r
  val VarBlock = """(data_variable)""".r
  val ParamBlock = """(argument_reporter_string_number|argument_reporter_boolean)""".r
  val AssignBlock = """(data_setvariableto|data_changevariableby)""".r

  override def toAst(): ScratchBlock = {
    val node: ScratchBlock = this.attr.get("type").get match {
      case IfElseBlock(opcode) => buildIfElseStmt(opcode)
      case LoopBlock(opcode) => buildLoopBlock(opcode)
      case ControlStopBlock(opcode) => buildStopStmt()
      case SimpleControlBlock(opcode) => buildSimpleBlock(new ExprStmt)
      case VarBlock(opcode) => buildVarExp() //need to match before expr block
      case ParamBlock(opcode) => buildVarExp()
      case ExprBlock(opcode) => buildSimpleBlock(new OperatorExpr)
      case AssignBlock(opcode) => buildAssignBlock()
      case StmtBlock(opcode) => buildSimpleBlock(new ExprStmt)
      case EventStartBlock(opcode) => buildSimpleBlock(new ExprStmt)
      case ProcCall(opcode) => buildProcAccess()
      case ReceiverBlock(opcode) => buildReceiveStmt()
      case BroadcastBlock(opcode) => buildBroadcastStmt()
      case _ => println("unknown block:", this); throw new Exception("Unknown block type")
    }
    node
  }

  def toBlockSeq(): BlockSeq = toBlockSeq(this)

  def toBlockSeq(start: BlockEl): BlockSeq = {
    val seq = new BlockSeq
    var blk = Option(start)
    do {
      try {
        seq.addStmt(blk.get.toAst().asInstanceOf[Stmt])
      } catch {
        //ignoring orphan expression blocks for now
        case e: ClassCastException =>
      }
      blk = blk.get.next
    } while (blk.isDefined)
    seq
  }

  private[this] def buildStopStmt(): ast.StopStmt = {
    val astNode = new ast.StopStmt
    astNode.setOpcode(attr.get("type").get)
    astNode.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava);
    astNode.initDefault(1)
    val default = new Default
    for (i <- 0 until args.size) args(i) match {
      case Some(f: Field) => {
        astNode.setArg(new AttrAccess(f.value), 0)
        default.setFieldAttr(f.attr); default.setFieldValue(f.value); (f.attr.get("name").get, f.value)
      }
      case Some(mut: Mutation) =>
      case _ => throw new Exception("")
    }
    astNode.setDefault(default, 0)

    astNode
  }

  private[this] def buildReceiveStmt(): ast.ReceiveStmt = {
    val astNode = new ast.ReceiveStmt
    astNode.setOpcode(this.attr.get("type").get)
    astNode.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava);

    val (name: String, id: String) = this.args.get(0) match {
      case Some(arg: Field) => (arg.value, arg.attr.get("id").get)
      case _ => throw new Exception("")
    }
    astNode.setMsg(new VarAccess(id))
    astNode.getMsg.setName(name)
    astNode.initDefault(1)
    val default = new Default
    default.setFieldAttr(this.args.get(0).get.asInstanceOf[Field].attr)
    default.setFieldValue(this.args.get(0).get.asInstanceOf[Field].value)
    astNode.setDefault(default, 0)

    astNode
  }

  private[this] def buildVarExp(): VarAccess = {
    val node = new VarAccess()
    node.setOpcode(this.attr.get("type").get)
    node.setBlockId(this.attr.get("id").get)
    node.initDefault(0)
    node.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava)
    val field: Field = this.args.get(0).get.asInstanceOf[Field]
    node.setName(field.value)
    if (node.getOpcode == "data_variable") {
      node.setID(field.attr.get("id").get)
    } else {
      node.setType(this.attr.get("type").get)
      // no ID for param read block; type = argument_reporter_string_number
    }
    node
  }

  private[this] def buildAssignBlock(): AssignStmt = {
    val astNode = new AssignStmt()
    astNode.setOpcode(this.attr.get("type").get)
    astNode.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava);
    astNode.initDefault(2)

    //var acc (dest)
    this.args.filter { case Some(arg) => arg.isInstanceOf[Field] }.collectFirst {
      case Some(arg: Field) => {
        astNode.setDest(new VarAccess(arg.attr.get("id").get))
        astNode.getDest.setName(arg.value)
      }
      case _ => throw new Exception("")
    }

    //expr source
    this.args.filter { case Some(arg) => arg.isInstanceOf[Value] }.collectFirst {
      case Some(Value(attr, shadow, block)) => {
        val default = if (shadow.isDefined) shadow.get.toDefault() else null
        val expr = if (block.isDefined) block.get.toAst().asInstanceOf[Expr] else default.toExpr()
        astNode.setDefault(default, 1)
        astNode.setSource(expr)
      }
      case _ =>
    }

    astNode
  }

  private[this] def buildBroadcastStmt(): BroadcastStmt = {
    val astNode = new BroadcastStmt()
    astNode.setOpcode(this.attr.get("type").get)
    astNode.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava);
    astNode.initDefault(1)
    this.args.get(0).get match {
      case Value(attr, shadow, block) => {
        val default = if (shadow.isDefined) shadow.get.toDefault() else null
        val expr = if (block.isDefined) block.get.toAst().asInstanceOf[Expr] else default.toExpr()
        astNode.setDefault(default, 0)
        astNode.setMsg(expr)
      }
      case _ =>
    }

    astNode
  }

  private[this] def buildProcAccess(): ProcAccess = {
    val astNode = new ProcAccess()
    astNode.setID(attr.get("id").get)
    astNode.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava);

    val mut = args(0).get.asInstanceOf[Mutation]
    val proccode = mut.attr.get("proccode").getOrElse(throw new Exception("proccode not defined"))
    val argids_lst = ParsingUtil.extractListOfString(mut.attr.get("argumentids").getOrElse(throw new Exception("argumentids not defined")))
    val argids = argids_lst.asJava //TODO: mutable list for java
    val meta = new ProcAccMeta(proccode, argids)
    astNode.setMeta(meta)

    //call args
    //skip the mutation
    val callargs = args.filter { case Some(a) => !a.isInstanceOf[Mutation] }
    if (callargs.size > 0) astNode.initDefault(this.args.size)
    for (i <- 0 until callargs.size) callargs(i) match {

      case Some(arg: Value) => arg.toExprNodeDefaultPair match {
        case (expr, default) =>
          astNode.setArgInput(expr, i); astNode.setDefault(default, i)
        case _ =>
      }
      case Some(arg: Field) => astNode.setArgInput(arg.toAst(), i)
      case None => //do nothing
      case _ => throw new Exception("Unknown arg")
    }
    astNode
  }

  private[this] def buildIfElseStmt(opcode: String): ScratchBlock = {
    val astNode = new IfElseStmt()
    astNode.setOpcode(attr.get("type").get)
    astNode.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava)
    astNode.initDefault(args.size)

    val argLen = Sb3Spec.getSpec(opcode).get.argMap.size
    for (i <- 0 until args.size) args(i) match {
      case Some(arg: Value) => {
        if (!arg.attr.get("name").get.startsWith("SUBSTACK")) {
          arg.toExprNodeDefaultPair match {
            case (expr, default) =>
              astNode.setCond(expr)
              astNode.setDefault(default, 0)
            case _ =>
          }
        }
      }
      case Some(arg: StatementEl) => {
        arg.attr.get("name") match {
          case Some("SUBSTACK") => astNode.setThen(arg.toAst())
          case Some("SUBSTACK2") => astNode.setElse(arg.toAst())
          case _ =>
        }
      }
      case _ =>
    }
    astNode
  }

  private[this] def buildLoopBlock(opcode: String): ScratchBlock = {
    val node = new LoopStmt()
    node.setOpcode(attr.get("type").get)
    node.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava)
    node.initDefault(args.size)
    if (args.isEmpty) {
      node.setBody(new BlockSeq)
    }

    val argLen = Sb3Spec.getSpec(opcode).get.argMap.size

    for (i <- 0 until args.size) args(i) match {
      case Some(arg: Value) => arg.toExprNodeDefaultPair match {
        case (expr, default) =>
          node.setCond(expr)
          node.setDefault(default, 0)
        case _ =>
      }
      case Some(arg: StatementEl) => {
        node.setBody(arg.toAst())
      }
      case None =>
      case _ => throw new Exception("Unknown Arg")
    }
    node
  }

  private[this] def buildSimpleBlock(node: SimpleBlock): SimpleBlock = {
    node.setOpcode(attr.get("type").get)
    node.setAttrs(collection.mutable.Map(attr.toSeq: _*).asJava)
    node.initDefault(args.size) //should follow spec?
    for (i <- 0 until this.args.size) {
      this.args(i) match {
        case Some(arg: Value) => arg.toExprNodeDefaultPair match {
          case (expr, default) =>
            val argidx = Sb3Spec.getArgPos(node.getOpcode, arg.attr.getOrElse("name", "__" + i))
            node.setArgInput(expr, argidx)
            node.setDefault(default, argidx)
          case _ =>
        }
        case Some(arg: Field) => {
          val argidx = Sb3Spec.getArgPos(node.getOpcode, arg.attr.getOrElse("name", "__" + i))
          node.setArgInput(arg.toAst(), argidx)
        }
        case None => //do nothing
        case other => throw new Exception("Unknown arg" + other)
      }
    }
    node
  }

  override def toXml() = {
    <block type={ optTxt(attr, "type") } id={ optTxt(attr, "id") } x={ optTxt(attr, "x") } y={ optTxt(attr, "y") }>
      { args.map(arg => if (arg.isDefined) arg.get.toXml) }
    </block>
  }

  def toBlockSeqXml(): scala.xml.Node = {
    <block type={ optTxt(attr, "type") } id={ optTxt(attr, "id") } x={ optTxt(attr, "x") } y={ optTxt(attr, "y") }>
      { args.map(arg => if (arg.isDefined) arg.get.toXml) }{ if (next.isDefined) <next>{ next.get.toBlockSeqXml() }</next> }
    </block>
  }
}

object Block2Ast {
  import scala.util.Random
  def optTxt(map: Map[String, String], key: String): Option[Text] = map.get(key).map(v =>
    if (v.isEmpty() || v.startsWith("__") || v == "[]") null else new Text(v))
  def reqTxt(map: Map[String, String], key: String, default: String): Option[Text] = Option(new Text(map.getOrElse(key, default)))
  def getRandomId() = Random.alphanumeric.take(6).mkString
}