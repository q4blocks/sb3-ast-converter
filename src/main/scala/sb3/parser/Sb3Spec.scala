package sb3.parser
import scala.io.Source
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.JsObject
import play.api.libs.json.Reads._

case class Sb3Spec(opcode: String, argMap: List[ArgSpec])
abstract class ArgSpec {
  val name: String
}
case class InputSpec(argType: String, inputOp: Option[String], name: String) extends ArgSpec
case class VariableInputSpec(argType: String, inputOp: Option[String], name: String, variableType: String) extends ArgSpec
case class FieldSpec(argType: String, name: String) extends ArgSpec
case class VariableFieldSpec(argType: String, name: String, variableType: String) extends ArgSpec
case class ArgPos(opcode: String, posMap: Map[String, Int])

object Sb3Spec {
  val stream = getClass.getResourceAsStream("/sb2-to-sb3-mapping.json")
  val source = scala.io.Source.fromInputStream(stream).getLines.mkString
  var json: JsValue = Json.parse(source)
  val map = json.as[JsObject].value
  var posMap = scala.collection.mutable.Map[String, ArgPos]()
  var specMap = scala.collection.mutable.Map[String, Sb3Spec]()

  implicit val inputArgSpecReads: Reads[InputSpec] = (
    (JsPath \ "type").read[String] and
    (JsPath \ "inputOp").readNullable[String] and
    (JsPath \ "inputName").read[String])(InputSpec.apply _)

  implicit val variableInputArgSpecReads: Reads[VariableInputSpec] = (
    (JsPath \ "type").read[String] and
    (JsPath \ "inputOp").readNullable[String] and
    (JsPath \ "inputName").read[String] and
    (JsPath \ "variableType").read[String])(VariableInputSpec.apply _)

  implicit val fieldArgSpecReads: Reads[FieldSpec] = (
    (JsPath \ "type").read[String] and
    (JsPath \ "fieldName").read[String])(FieldSpec.apply _)

  implicit val variableFieldArgSpecReads: Reads[VariableFieldSpec] = (
    (JsPath \ "type").read[String] and
    (JsPath \ "fieldName").read[String] and
    (JsPath \ "variableType").read[String])(VariableFieldSpec.apply _)

  for ((k, v) <- map) {
    val argMap = v("argMap").as[JsArray].value.map(_.as[JsObject]).map(arg => {
      (arg("type").as[String], arg.keys.contains("variableType")) match {
        case ("input", false) => arg.validate[InputSpec].get
        case ("input", true)  => arg.validate[VariableInputSpec].get
        case ("field", false) => arg.validate[FieldSpec].get
        case ("field", true)  => arg.validate[VariableFieldSpec].get
        case _                => throw new Exception("Unknown Arg type")
      }
    })
    val sb3opcode = v("opcode").as[String]
    specMap(sb3opcode) = Sb3Spec(sb3opcode, argMap.toList)
    posMap(sb3opcode) = getArgToPosMap(sb3opcode)
  }

  def inferSpec(opcode: String, args: List[Arg]):Option[Sb3Spec] = {
    def extractSpec(args: List[Arg]): List[InputSpec] = {
      var argSpecList: List[InputSpec] = List()
      for(i <- 0 until args.size){
        argSpecList = argSpecList :+ (args(i) match {
            case arg: Value => InputSpec("input", None, "__"+i.toString())
            case arg: Field => InputSpec("field", None, "__"+i.toString())
          })
      }
      argSpecList
    }
    val spec = Sb3Spec(opcode, extractSpec(args))
    specMap(opcode) = spec
    posMap(opcode) = getArgToPosMap(opcode)
    Option(spec)    
  }

  def getSpec(opcode: String): Option[Sb3Spec] = {
    specMap.get(opcode)
  }

  def getArgSpec(opcode: String, idx: Int): Option[ArgSpec] = {
    try {
      Some(getSpec(opcode).get.argMap(idx))
    } catch {
      case e: Exception => None
    }
  }

  def getArgName(opcode: String, idx: Int): Option[String] = {
    getArgSpec(opcode, idx) match {
      case Some(argSpec) => Some(argSpec.name)
      case None          => None
    }
  }

  def getArgPos(opcode: String, argName: String): Int = {
    posMap.get(opcode) match {
      case Some(argPos) => {
        argPos.posMap.get(argName) match {
          case Some(idx) => idx
          case _         => -1
        }
      }
      case _ => -1
    }
  }

  def getArgToPosMap(opcode: String): ArgPos = {
    val spec = getSpec(opcode).get
    val inputNames = spec.argMap.map(el => {
      el match {
        case el: InputSpec         => el.name
        case el: VariableInputSpec => el.name
        case el: FieldSpec         => el.name
        case el: VariableFieldSpec => el.name
      }
    })
    val map = (inputNames.indices zip inputNames).map(_.swap).toMap
    ArgPos(opcode, map)
  }

}

