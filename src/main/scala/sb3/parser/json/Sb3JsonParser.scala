package sb3.parser.json
import ast.{ List => ASTList, _ }

import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.Reads.BooleanReads
import play.api.libs.json.Reads.JsArrayReads
import play.api.libs.json.Reads.JsObjectReads
import play.api.libs.json.Reads.JsStringReads
import play.api.libs.json.Reads.StringReads
import sb3.parser.Arg
import sb3.parser.BlockEl
import sb3.parser.Field
import sb3.parser.Shadow
import sb3.parser.Value
import sb3.parser._

class Sb3Json2Block {
  def programEl(json: JsValue): ProgramEl = {
    val stage = stageEl((json \ "targets").get.as[JsArray])
    ProgramEl(stage)
  }

  def stageEl(targets: JsArray): StageEl = {
    val sprites = targets.value.filter { jsVal => (jsVal \ "isStage").as[Boolean] == false }.map(tar => spriteEl(tar.as[JsObject]))
    StageEl(vars = List.empty[VariableEl], scripts = List.empty[ScriptEl], sprites = sprites.toList)
  }

  def spriteEl(jsObj: JsObject): SpriteEl = {
    val blocks = (jsObj \ "blocks").as[JsObject]
    val scriptList: List[ScriptEl] = scriptListEl(Json.toJson(blocks.value.filter { case (id, jsVal) => (jsVal \ "opcode").as[String] != "procedures_definition" }).as[JsObject])
    val procDefList: List[ProcDefEl] = procDefListEl(blocks)
    SpriteEl(name = "", procDefs = procDefList, scripts = scriptList)
  }

  def scriptListEl(blockMap: JsObject): List[ScriptEl] = {
    var scripts = List.empty[ScriptEl]
    val topLevels = blockMap.value.filter { case (id, jsObj) => (jsObj \ "topLevel").as[Boolean] }
    //    assert(!topLevels.isEmpty)
    for ((topLevBlockId, topLevJsObj) <- topLevels) {
      val script = scriptEl(topLevBlockId, blockMap)
      scripts = script +: scripts
    }
    scripts
  }

  def procDefListEl(blockMap: JsObject): List[ProcDefEl] = {
    var procDefs = List.empty[ProcDefEl]
    val topLevels = blockMap.value.filter { case (id, jsObj) => ((jsObj \ "topLevel").as[Boolean] && (jsObj \ "opcode").as[String] == "procedures_definition") }
    //    assert(!topLevels.isEmpty)
    for ((topLevBlockId, topLevJsObj) <- topLevels) {
      val procDef = procDefEl(topLevBlockId, blockMap)
      procDefs = procDef +: procDefs
    }
    procDefs
  }

  def scriptEl(topBlockId: String, blockMap: JsObject): ScriptEl = {
    ScriptEl(blockSeqEl(topBlockId, blockMap))
  }

  def procDefEl(topBlockId: String, blockMap: JsObject): ProcDefEl = {
    val currBlockJsObj = (blockMap \ topBlockId).as[JsObject]
    val nextRes = (currBlockJsObj \ "next")

    val attr = Map("type" -> (currBlockJsObj \ "opcode").as[String], "id" -> topBlockId)
    val input = (currBlockJsObj \ "inputs" \ "custom_block").as[JsArray].apply(1)
    val proto = (blockMap \ input.as[String])
    val mutation = (proto \ "mutation")
    val sigArgs: List[Arg] = List(Mutation(Map("proccode" -> (mutation \ "proccode").as[String], "argumentids" -> (mutation \ "argumentids").as[String],
      "argumentnames" -> (mutation \ "argumentnames").as[String], "argumentdefaults" -> (mutation \ "argumentdefaults").as[String], "warp" -> (mutation \ "warp").as[String])))
    val signature = Value(Map("name" -> "custom_block"), shadow = Some(Shadow(Map("type" -> (proto \ "opcode").as[String], "id" -> input.as[String]), args = sigArgs)))
    val seq: Option[BlockSeqEl] = if (nextRes.asOpt[String].isDefined) {
      Option(blockSeqEl(nextRes.as[String], blockMap))
    } else None
    ProcDefEl(attr, signature, seq)
  }

  def blockSeqEl(topBlockId: String, blockMap: JsObject): BlockSeqEl = {
    def blockJsonItr(currBlockId: String, blockMap: JsObject): BlockEl = {
      val currBlockJsObj = (blockMap \ currBlockId).as[JsObject]
      val opcode = (currBlockJsObj \ "opcode").as[String]
      val attr = Map("type" -> opcode, "id" -> currBlockId)
      val blockJsObj = (blockMap \ currBlockId).as[JsObject]
      val rawArgs = ExtractorHelper.extractInputList(blockJsObj, blockMap)
      var inputArgs = rawArgs.map(a => Option(argEl(a, blockMap)))
      val fieldArgs = ExtractorHelper.extractFieldList(blockJsObj, blockMap).filter(arg => arg != None)
      var args = List.concat(inputArgs,fieldArgs)
      if((currBlockJsObj \ "opcode").as[String] == "procedures_call" && args.isEmpty){
        val mutation = (currBlockJsObj \ "mutation")
        args = List(Option(Mutation(Map("proccode" -> (mutation \ "proccode").as[String], "argumentids" -> (mutation \ "argumentids").as[String],
           "warp" -> (mutation \ "warp").as[String]))))
      }
      val nextRes = (currBlockJsObj \ "next")
      val next: Option[BlockEl] = if (nextRes.asOpt[String].isDefined) {
        Option(blockJsonItr(nextRes.as[JsString].value, blockMap))
      } else None

      BlockEl(attr, args, next)
    }

    val topBlock = blockJsonItr(topBlockId, blockMap)
    BlockSeqEl(topBlock)
  }

  def blockEl(blockId: String, blockJsObj: JsObject, blockMap: JsObject): BlockEl = {
    val attr = Map("type" -> (blockJsObj \ "opcode").as[String], "id" -> blockId)
    val rawArgs = ExtractorHelper.extractInputList(blockJsObj, blockMap)
    var inputArgs = rawArgs.map(a => Option(argEl(a, blockMap)))
    val fieldArgs = ExtractorHelper.extractFieldList(blockJsObj, blockMap).filter(arg => arg != None)
    if((blockJsObj \ "opcode").as[String] == "procedures_call"){
      val mutation = (blockJsObj \ "mutation")
      val mutationArgs = List(Option(Mutation(Map("proccode" -> (mutation \ "proccode").as[String], "argumentids" -> (mutation \ "argumentids").as[String],
       "warp" -> (mutation \ "warp").as[String]))))
      inputArgs = List.concat(inputArgs,mutationArgs)
    }
    var args = List.concat(inputArgs,fieldArgs)
    BlockEl(attr, args, None)
  }

  def argEl(input: Input, blockMap: JsObject): Arg = {
    input match {
      case Input("SUBSTACK", "","", blockIdOpt) => {
        val id: String = blockIdOpt.getOrElse("null")
        if (id.equals("null")) {
          StatementEl(Map("name" -> "SUBSTACK"), None)
        } else {
          val currBlockJsObj = (blockMap \ id).as[JsObject]
          val block = blockEl(id, currBlockJsObj, blockMap)
          StatementEl(Map("name" -> "SUBSTACK"), Some(block))
        }
      }
      case Input("SUBSTACK2", "","", blockIdOpt) => {
        val id: String = blockIdOpt.getOrElse("null")
        if (id.equals("null")) {
          StatementEl(Map("name" -> "SUBSTACK2"), None)
        } else {
          val currBlockJsObj = (blockMap \ id).as[JsObject]
          val block = blockEl(id, currBlockJsObj, blockMap)
          StatementEl(Map("name" -> "SUBSTACK2"), Some(block))
        }
      }
      case Input(name, shadowVal,"", None) => {
        val shad = Shadow(Map(), List(Field(Map(), shadowVal)))
        Value(Map("name" -> name), Some(shad), None)
      }
      case Input(name, shadowVal,"", blockIdOpt) => {
        val id: String = blockIdOpt.get
        val currBlockJsObj = (blockMap \ id).as[JsObject]
        val shad = Shadow(Map(), List(Field(Map(), shadowVal)))
        val block = blockEl(id, currBlockJsObj, blockMap)
        Value(Map("name" -> name), Some(shad), Some(block))
      }
      case Input(name, "",shadowBlock, None) => {
        val id: String = shadowBlock
        val currBlockJsObj = (blockMap \ id).as[JsObject]
        val fieldVal = (currBlockJsObj \ "fields" \ name).as[JsArray].value.apply(0).as[String]
        val shad = Shadow(Map("type" -> (currBlockJsObj \ "opcode").as[String], "id" -> id), List(Field(Map("name"->name), fieldVal)))
        Value(Map("name"->name), Some(shad), None)
      }
      case Input(name, "",shadowBlock, blockIdOpt) => {
        val id: String = shadowBlock
        val currBlockJsObj = (blockMap \ id).as[JsObject]
        val fieldVal = (currBlockJsObj \ "fields" \ name).as[JsArray].value.apply(0).as[String]
        val shad = Shadow(Map("type" -> (currBlockJsObj \ "opcode").as[String], "id" -> id), List(Field(Map("name"->name), fieldVal)))
        val currBlockJsObj2 = (blockMap \ id).as[JsObject]
        val block = blockEl(id, currBlockJsObj2, blockMap)
        Value(Map("name"->name), Some(shad), Some(block))
      }
    }
  }

  def parseAsProgram(jsonStr: String): Program = {
    val jsVal = Json.parse(jsonStr)
    val programEl = new Sb3Json2Block().programEl(jsVal)
    programEl.toAst()
  }
}

object Sb3Json2Block extends App {  
   val jsonstr = """
    {"targets":[{"isStage":true,"name":"Stage","variables":{"`jEk@4|i[#Fk?(8x)AV.-my variable":["my variable",0]},"lists":{},"broadcasts":{},"blocks":{},"currentCostume":0,"costumes":[{"assetId":"cd21514d0531fdffb22204e0ec5ed84a","name":"backdrop1","md5ext":"cd21514d0531fdffb22204e0ec5ed84a.svg","dataFormat":"svg","rotationCenterX":1,"rotationCenterY":1}],"sounds":[{"assetId":"83a9787d4cb6f3b7632b4ddfebf74367","name":"pop","dataFormat":"wav","format":"","rate":44100,"sampleCount":1032,"md5ext":"83a9787d4cb6f3b7632b4ddfebf74367.wav"}],"volume":100,"tempo":60,"videoTransparency":50,"videoState":"off"},{"isStage":false,"name":"Sprite1","variables":{},"lists":{},"broadcasts":{},"blocks":{"@/O)M4y8_($x~d]L}$U6":{"opcode":"motion_movesteps","next":"M?{T{,6@P8:wyMb;`X*M","parent":null,"inputs":{"STEPS":[3,"4qq%ah84}j5|}qj1ng+p",[4,"10"]]},"fields":{},"topLevel":true,"shadow":false,"x":412,"y":191},"4qq%ah84}j5|}qj1ng+p":{"opcode":"operator_random","next":null,"parent":"@/O)M4y8_($x~d]L}$U6","inputs":{"FROM":[1,[4,"1"]],"TO":[1,[4,"10"]]},"fields":{},"topLevel":false,"shadow":false},"M?{T{,6@P8:wyMb;`X*M":{"opcode":"motion_turnright","next":null,"parent":"@/O)M4y8_($x~d]L}$U6","inputs":{"DEGREES":[1,[4,"15"]]},"fields":{},"topLevel":false,"shadow":false}},"currentCostume":0,"costumes":[{"assetId":"09dc888b0b7df19f70d81588ae73420e","name":"costume1","bitmapResolution":1,"md5ext":"09dc888b0b7df19f70d81588ae73420e.svg","dataFormat":"svg","rotationCenterX":47,"rotationCenterY":55},{"assetId":"3696356a03a8d938318876a593572843","name":"costume2","bitmapResolution":1,"md5ext":"3696356a03a8d938318876a593572843.svg","dataFormat":"svg","rotationCenterX":47,"rotationCenterY":55}],"sounds":[{"assetId":"83c36d806dc92327b9e7049a565c6bff","name":"Meow","dataFormat":"wav","format":"","rate":44100,"sampleCount":37376,"md5ext":"83c36d806dc92327b9e7049a565c6bff.wav"}],"volume":100,"visible":true,"x":0,"y":0,"size":100,"direction":90,"draggable":false,"rotationStyle":"all around"}],"meta":{"semver":"3.0.0","vm":"0.1.0-prerelease.1527804956","agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36"}}
    """
  val jsVal = Json.parse(jsonstr)
  val formattedJs = Json.prettyPrint(jsVal)
  println(formattedJs)

  val res = new Sb3Json2Block().programEl(jsVal)
  println(res)
  val xml = res.stage.sprites(0).scripts(0).seq.toXml()
  println(xml)
  val seq = res.stage.sprites(0).scripts(0).seq.toAst()
  val script = new Script(seq)
  println(script.ASTDotSpec())

  // json to block should produce similar block structure/attributes as the one parsed from xml
  val equivalentXml = <block type="motion_movesteps" id="MN+Y_L8lS~.G/g$xm_0G" x="184" y="217"><value name="STEPS"><shadow type="math_number" id="x;?;hyJzIy%R!P/0ndtq"><field name="NUM">10</field></shadow><block type="operator_random" id="K{4bdT%bJ#@N5s)8Q!Qn"><value name="FROM"><shadow type="math_number" id="#UAs=(9zsXBfg+O$_Bpk"><field name="NUM">1</field></shadow></value><value name="TO"><shadow type="math_number" id="G~bVR-h}A)bLPGCr+OX|"><field name="NUM">10</field></shadow></value></block></value><next><block type="motion_turnright" id="Xs$rq0[+=v9q-Jas9Fqm"><value name="DEGREES"><shadow type="math_number" id=".nyl*$:)hfn1Oo8$5elJ"><field name="NUM">15</field></shadow></value></block></next></block>
  val parser = new sb3.parser.xml.Sb3XmlParser()
  val expectedBlockStructure = parser.parseBlock(equivalentXml)
  println(expectedBlockStructure)
  val expectedAst = new Script(expectedBlockStructure.toBlockSeq())
  println(expectedAst.ASTDotSpec())
  import sb3.parser.Ast2ProgramElems.blockSeq2ConnectedBlock
  println(expectedAst.getBody.toXml())
  assert(scala.xml.Utility.trim(expectedAst.getBody.toXml()) == equivalentXml)
}
